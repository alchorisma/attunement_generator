#!/usr/bin/python

import csv
from pathlib import Path
import random 
import os
import subprocess

## FUNCTIONS
# for each word it finds the relative chakra levels
def finding_chakras(issue, issues):
	number = 1
	for wordlist in issues.values(): #for list of words in dictionary
		if issue in wordlist: # if word is in list of words
			chakra = str(number)
		number += 1
	return chakra

# import csv file with values into list of dictionaries, each row represents dictionary with characteristics
def reading_data_as_list_of_dict(csv_file):
	data = []
	with open(csv_file, mode='r') as source:
		# read row
		for line in csv.DictReader(source):
			# add elements of the row as dictionary to list data 
			data.append(line)
	return data

# convert list of tuples to dictionary
def tuple_to_dict(list):
	new_dict = {}
	for issue, level in list: 
		new_dict[issue] = level
	return new_dict


## IMPORT DATA

# import all values related to trees, chakras, mouvement, sounds...
csv_file = 'data_attunement_generator.csv'
data_general = reading_data_as_list_of_dict(csv_file)
#print(data_general)

# import words related to chakras
lists_of_tuples = []
# declare path
path = r"wordings/"
# path is a directory of which you want to list
for txt in sorted(os.listdir(path)):
	with open(path+txt) as source:
		data=[tuple(line) for line in csv.reader(source)]
		lists_of_tuples.append(data)

# convert tuples into dictionaries 
all_levels =[]

chakra_level_1 = lists_of_tuples[0]
level_1 = tuple_to_dict(chakra_level_1)
all_levels.append(level_1)

chakra_level_2 = lists_of_tuples[1]
level_2 = tuple_to_dict(chakra_level_2)
all_levels.append(level_2)

chakra_level_3 = lists_of_tuples[2]
level_3 = tuple_to_dict(chakra_level_3)
all_levels.append(level_3)

chakra_level_4 = lists_of_tuples[3]
level_4 = tuple_to_dict(chakra_level_4)
all_levels.append(level_4)

chakra_level_5 = lists_of_tuples[4]
level_5 = tuple_to_dict(chakra_level_5)
all_levels.append(level_5)

chakra_level_6 = lists_of_tuples[5]
level_6 = tuple_to_dict(chakra_level_6)
all_levels.append(level_6)

chakra_level_7 = lists_of_tuples[6]
level_7 = tuple_to_dict(chakra_level_7)
all_levels.append(level_7)

#print(all_levels)


if __name__ == "__main__":
	from pydub import AudioSegment 
	from pydub.playback import play

	## QUESTIONS
	while 1==1:
		print("Hello, this attunement generator is here to try to give you some tools to improve a situation that is hindering to fully enjoy the here and the now.")
		print("Would you prefer to meet a tree (1), work with a chakra (2), perform a mouvement (3), or sing a mantra (4)?")
		chosen_form = int(input("Please enter a number: "))

		# Find 1st chakra level
		print("Please indicate the realm of your question.")
		print("Do you have a question regarding survival (1), sexuality (2), self-esteem (3), love (4), communication (5), insight (6), a connection to other dimensions (7) or random (8)?")
		print('\n')
		chakra_1 = int(input("Please enter a number: "))
		if chakra_1 == 8:
			chakra_1 = random.randint(1,7)

		# Print word cloud of chosen chakra 1
		question_level_1 = {'1':"SURVIVAL", '2':"SEXUALITY", '3':"SELF-ESTEEM", '4':"LOVE", "5":"COMMUNICATION", "6": "INSIGHT", "7": "CONNECTION TO OTHER DIMENSIONS"}
		words_2_print = all_levels[chakra_1-1]
		title = question_level_1[str(chakra_1)]
		print('\n')
		print(title,'- the realm of your question contains the following word cloud: ')
		print('\n')
		print(', '.join(words_2_print))
		print('\n')

		# Choose two words that specify your question more
		print("Please choose two words that relate to your question.\n")

		issue_1 = input("The first word is: ")
		issue_2 = input("The second word is: ")


		# Find 2nd and 3rd chakra levels related to chosen issues

		chakra_2 = words_2_print[issue_1]
		# print('chakra_2', chakra_2)
		chakra_3 = words_2_print[issue_2]
		# print('chakra_3', chakra_3)
		

		## FORMS

		# pick randomly row of data depending on chakra number
		selection_1 = []
		selection_2 = []
		selection_3 = []
		# pick randomly row of data depending on chakra_1
		for dict in data_general:
			if dict['chakra_number'] == str(chakra_1):
				selection_1.append(dict)
		choice_1 = random.choice(selection_1)
		# pick randomly row of data depending on chakra_2
		for dict in data_general:
			if dict['chakra_number'] == str(chakra_2):
				selection_2.append(dict)
		choice_2 = random.choice(selection_2)
		# pick randomly row of data depending on chakra_3
		for dict in data_general:
			if dict['chakra_number'] == str(chakra_3):
				selection_3.append(dict)
		choice_3 = random.choice(selection_3)
		chakra_2 = int(chakra_2)
		chakra_3 = int(chakra_3)

		# chosen form is a tree
		if chosen_form == 1:
			print("\n")
			print("TREE")
			print("\n")
			print("get image", choice_1['image_species'])
			print("\n")
			print("The name of the tree that might help you with your question, is: ", choice_1['species_english'])
			print("Le nom de l'arbre qui pourrait vous aider avec votre question, est: ", choice_1['species_french'])
			print("Nazwa drzewa, które może ci pomóc w twoim pytaniu, to: ", choice_1['species_polish'])
			print("De naam van de boom die je kan helpen met je vraag, is: ", choice_1['species_dutch'])
			print("The Latin name of the tree that might help you, is: ", choice_1['species_latin'])
			print("\n")
			if choice_1['celtic_use']:
				print("In the Celtic tradition the tree was approached for the following reasons: ", choice_1['celtic_use'])
			if choice_1['celtic_value_1']:
				print("In the Celtic tradition the tree was symbol for: ", choice_1['celtic_value_1'])
			if choice_1['celtic_value_2']:
				print("In the Celtic tradition the tree was also symbol for: ", choice_1['celtic_value_2'])
			if choice_1['celtic_value_3']:
				print("In the Celtic tradition the tree was also symbol for: ", choice_1['celtic_value_3'])
			print("\n")
			if choice_1['edibility']:
				print("Some parts of this tree are edible: ", choice_1['edibility'])
			if choice_1['characteristics']:
				print("More characteristics: ", choice_1['characteristics'])
			print("\n")
			print("ADDITIONAL ATTUNEMENT ELEMENTS")
			print("\n")
			print("The colour that might help you with your question, is", choice_1['chakra_colour'])
			print("\n")
			print("The sound that might help you with your question, is", choice_1['chakra_bija_mantra'])


		# chosen form is chakra
		elif chosen_form == 2:
			print("\n")
			print("CHAKRA")
			print("\n")
			print("The chakra that might help you with your question is, in English: ", choice_1['chakra_type_english'])
			print("In Sanskrit it is named: ", choice_1['chakra_type_sanskrit'])
			print("The location of the chakra is", choice_1['chakra_location'])
			print("\n")
			print("The first quality that is related to this chakra, is", choice_1['chakra_quality_1'])
			print("The second quality that is related to this chakra, is ", choice_1['chakra_quality_2'])
			print("The third quality that is related to this chakra, is", choice_1['chakra_quality_3'])
			print("\n")
			print("The colour you can work with is", choice_1['chakra_colour'])
			print("The stones that activate this chakra:", choice_1['chakra_stone'])
			print("The element that activates this chakra:", choice_1['chakra_element'])
			print("The action that activates this chakra:", choice_1['chakra_action'])
			print("The gland that is activated through this chakra: ", choice_1['chakra_gland'])
			print("\n")
			print("ADDITIONAL ATTUNEMENT ELEMENTS")
			print("\n")
			print("The planet that might help you with your question, is", choice_2['planet'])
			print("get image", choice_2['image_planet'])
			print("\n")
			print("The tarot card that might help you with your question, is", choice_3['tarot_card'])
			print("get image", choice_3['image_tarot'])

		# chosen form is mouvement
		elif chosen_form == 3:
			print("\n")
			print("EXERCISE")
			print("\n")
			if choice_1['rune_primary']:
				print("The exercise(s) that might help you, is:\n ")
				print(choice_1['rune_primary'])
				print("\n")
				print(choice_1['image_rune_primary'])
				print("\n")
				print(choice_1['mouvement_primary'])
				print("\n")

			if choice_1['rune_secondary']:
				print("OR:")
				print("\n")
				print(choice_1['rune_secondary'])
				print("\n")
				print(choice_1['image_rune_secondary'])
				print("\n")
				print(choice_1['mouvement_secondary'])
				print("\n")
			
			print("ADDITIONAL ATTUNEMENT ELEMENTS")
			print("\n")
			print("The stones that might help you with this exercise, are:", choice_2['chakra_stone'])
			print("\n")
			print("get image", choice_2['image_chakra_stone'])
			print("\n")
			print("You might want to wear clothes of the colour", choice_3['chakra_colour'])



		# chosen form is mantra
		elif chosen_form == 4:
			frequency = choice_1['chakra_frequency']
			sound_frequency = choice_1['sound_frequency']
			print(chakra_1, frequency, sound_frequency)
			primal = choice_1['chakra_primal_sound']
			sound_primal = choice_1['recording_primal']
			print(chakra_1, primal, sound_primal)
			bija = choice_2['chakra_bija_mantra']
			sound_bija = choice_2['recording_bija']
			print(chakra_2, bija, sound_bija)
			vowel = choice_3['chakra_vowel']
			sound_vowel = choice_3['recording_vowel']
			print(chakra_3, vowel, sound_vowel)
			solfeggio = choice_3['chakra_solfeggio']
			sound_solfeggio = choice_3['recording_solfeggio']
			print(chakra_3, solfeggio, sound_solfeggio)

			print("\n")
			print("MANTRA")
			print("\n")
			print("The primal sound that relates to your question, is: ", primal)
			print("The Bija mantra that resonates with your question, is: ", bija)
			print("The vowel that resonates with your question, is: ", vowel)
			print("The solfeggio that resonates with your question, is: ", solfeggio)

			# create song based on individual sounds
			song = [sound_primal, sound_bija, sound_vowel, sound_solfeggio]

			# generate song
			subprocess.call(['sox'] + song + ['out.mp3'])
			new_song = AudioSegment.from_mp3('out.mp3')

			# play song
			play(new_song)

			print("\n")
			print("ADDITIONAL ATTUNEMENT ELEMENT")
			print("\n")
			print("The frequency that resonates with your question, is: ", frequency)
			print("\n")
			hum_to_frequency = input("Would you also like to hum to the frequency that resonates with your question? Type y or n.\n")
			if hum_to_frequency == "y":
				frequency_song = AudioSegment.from_mp3(sound_frequency)
				play(frequency_song)
			else:
				pass

			

		# raise error
		else:
			print("The number or words you typed does not correspond to any of the proposed forms. Please try again.")

		print("\n\n")
		input("Press any key to start again.")
		print("\n\n")
