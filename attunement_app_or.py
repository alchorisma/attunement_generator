from attunement_wording import *

from flask import Flask
from flask import render_template
from flask import request
import random

app = Flask(__name__)

@app.route('/')
def index(name=None):
	return render_template('index.html')

# @app.route('/step/<step>')
# def step(step=None):
#     return render_template(f'step{step}.html', **request.args)

@app.route('/step/1')
def step1():
	return render_template(f'step1.html', **request.args)

@app.route('/step/2')
def step2():
	return render_template(f'step2.html', **request.args)

@app.route('/step/3')
def step3 ():
	question_level_1 = {'1':"SURVIVAL", '2':"SEXUALITY", '3':"SELF-ESTEEM", '4':"LOVE", "5":"COMMUNICATION", "6": "WILL", "7": "CONNECTION TO OTHER DIMENSIONS"}
	ctx = dict(request.args)
	chakra_1 = int(ctx['chakra_1'])
	if chakra_1 == 8:
		ctx['chakra_1'] = chakra_1 = random.randint(1,7)
	ctx['words_2_print'] = all_levels[chakra_1-1]
	ctx['title'] = question_level_1[str(chakra_1)]
	return render_template(f'step3.html', **ctx)

@app.route('/step/4')
def step4():
	ctx = dict(request.args) # request.args is the FORM data (all the inputs up til now)
	# ctx['issues'] = issues = [int(x) for x in request.args.getlist('issues')]
	ctx['issues'] = issues = [x for x in request.args.getlist('issues')]
	issue_1, issue_2 = issues
	chakra_1 = int(ctx['chakra_1'])
	ctx['words_2_print'] = words_2_print = all_levels[chakra_1-1]
	print ("words_2_print", words_2_print)
	ctx['chakra_2'] = chakra_2 = words_2_print[issue_1] # issue_1 == 2
	print('chakra_2', chakra_2)
	chakra_3 = words_2_print[issue_2]
	print('chakra_3', chakra_3)	

	selection_1 = []
	selection_2 = []
	selection_3 = []
	# pick randomly row of data depending on chakra_1
	for d in data_general:
		if d['chakra_number'] == str(chakra_1):
			selection_1.append(d)
	ctx['choice_1'] = choice_1 = random.choice(selection_1)
	ctx['image_species'] = [x.strip() for x in choice_1['image_species'].split(',')]
	ctx['image_rune'] = [x.strip() for x in choice_1['image_rune'].split(',')]
	# pick randomly row of data depending on chakra_2
	for d in data_general:
		if d['chakra_number'] == str(chakra_2):
			selection_2.append(d)
	ctx['choice_2'] = choice_2 = random.choice(selection_2)
	# pick randomly row of data depending on chakra_3
	for d in data_general:
		if d['chakra_number'] == str(chakra_3):
			selection_3.append(d)
	ctx['choice_3'] = choice_3 = random.choice(selection_3)
	ctx['chakra_2'] = chakra_2 = int(chakra_2)
	ctx['chakra_3'] = chakra_3 = int(chakra_3)
	ctx['image_planet'] = image_planet = choice_2['image_planet']
	ctx['image_tarot'] = image_tarot = choice_3['image_tarot']
	ctx['image_chakra_stone'] = images_chakra_stone = [x.strip() for x in choice_2['image_chakra_stone'].split(',')]
	ctx['chakra_frequency'] = frequency = choice_1['chakra_frequency']
	ctx['sound_frequency'] = sound_frequency = choice_1['sound_frequency']
	ctx['primal'] = primal = choice_1['chakra_primal_sound']
	ctx['sound_primal'] = sound_primal = choice_1['recording_primal']
	ctx['bija'] = bija = choice_2['chakra_bija_mantra']
	ctx['sound_bija'] = sound_bija = choice_2['recording_bija']
	ctx['vowel'] = vowel = choice_3['chakra_vowel']
	ctx['sound_vowel'] = sound_vowel = choice_3['recording_vowel']
	ctx['solfeggio'] = solfeggio = choice_3['chakra_solfeggio']
	ctx['sound_solfeggio'] = sound_solfeggio = choice_3['recording_solfeggio']
	ctx['song'] = song = [sound_primal, sound_bija, sound_vowel, sound_solfeggio]
	
	chosen_form = int(ctx['form'])
	# chosen form is a tree
	if chosen_form == 1:
		# print("\n")
		# print("TREE")
		# print("\n")
		# print("get image", choice_1['image_species'])
		# print("\n")
		# print("The name of the tree that might help you with your question, is: ", choice_1['species_english'])
		# print("Le nom de l'arbre qui pourrait vous aider avec votre question, est: ", choice_1['species_french'])
		# print("Nazwa drzewa, które może ci pomóc w twoim pytaniu, to: ", choice_1['species_polish'])
		# print("De naam van de boom die je kan helpen met je vraag, is: ", choice_1['species_dutch'])
		# print("The Latin name of the tree that might help you, is: ", choice_1['species_latin'])
		# print("\n")
		# if choice_1['celtic_use']:
		# 	print("In the Celtic tradition the tree was approached for the following reasons: ", choice_1['celtic_use'])
		# if choice_1['celtic_value_1']:
		# 	print("In the Celtic tradition the tree was symbol for: ", choice_1['celtic_value_1'])
		# if choice_1['celtic_value_2']:
		# 	print("In the Celtic tradition the tree was also symbol for: ", choice_1['celtic_value_2'])
		# if choice_1['celtic_value_3']:
		# 	print("In the Celtic tradition the tree was also symbol for: ", choice_1['celtic_value_3'])
		# print("\n")
		# if choice_1['edibility']:
		# 	print("Some parts of this tree are edible: ", choice_1['edibility'])
		# if choice_1['characteristics']:
		# 	print("More characteristics: ", choice_1['characteristics'])
		# print("\n")
		# print("ADDITIONAL ATTUNEMENT ELEMENTS")
		# print("\n")
		# print("The colour that might help you with your question, is", choice_1['chakra_colour'])
		# print("\n")
		# print("The sound that might help you with your question, is", choice_1['chakra_bija_mantra'])
		return render_template(f'step4_1.html', **ctx)


	# chosen form is chakra
	elif chosen_form == 2:
		
		return render_template(f'step4_2.html', **ctx)

	# chosen form is mouvement
	elif chosen_form == 3:
		
		return render_template(f'step4_3.html', **ctx)

	# chosen form is mantra
	elif chosen_form == 4:
		# frequency = choice_1['chakra_frequency']
		# sound_frequency = choice_1['sound_frequency']
		# print(chakra_1, frequency, sound_frequency)
		# primal = choice_1['chakra_primal_sound']
		# sound_primal = choice_1['recording_primal']
		# print(chakra_1, primal, sound_primal)
		# bija = choice_2['chakra_bija_mantra']
		# sound_bija = choice_2['recording_bija']
		# print(chakra_2, bija, sound_bija)
		# vowel = choice_3['chakra_vowel']
		# sound_vowel = choice_3['recording_vowel']
		# print(chakra_3, vowel, sound_vowel)
		# solfeggio = choice_3['chakra_solfeggio']
		# sound_solfeggio = choice_3['recording_solfeggio']
		# print(chakra_3, solfeggio, sound_solfeggio)

		# print("\n")
		# print("MANTRA")
		# print("\n")
		# print("The primal sound that relates to your question, is: ", primal)
		# print("The Bija mantra that resonates with your question, is: ", bija)
		# print("The vowel that resonates with your question, is: ", vowel)
		# print("The solfeggio that resonates with your question, is: ", solfeggio)

		# # create song based on individual sounds
		# song = [sound_primal, sound_bija, sound_vowel, sound_solfeggio]

		# # generate song
		# subprocess.call(['sox'] + song + ['out.mp3'])
		# new_song = AudioSegment.from_mp3('out.mp3')

		# # play song
		# play(new_song)

		# print("\n")
		# print("ADDITIONAL ATTUNEMENT ELEMENT")
		# print("\n")
		# print("The frequency that resonates with your question, is: ", frequency)
		# print("\n")
		# hum_to_frequency = input("Would you also like to hum to the frequency that resonates with your question? Type y or n.\n")
		# if hum_to_frequency == "y":
		# 	frequency_song = AudioSegment.from_mp3(sound_frequency)
		# 	play(frequency_song)
		# else:
		# 	pass
		return render_template(f'step4_4.html', **ctx)

		
@app.route('/step/5')
def step5():








