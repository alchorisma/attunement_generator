
import csv
from pathlib import Path
import random 
import os
import subprocess
from pydub import AudioSegment 
from pydub.playback import play

# import csv file with values into list of dictionaries, each row represents dictionary with characteristics
def reading_data_as_list_of_dict(csv_file):
	data = []
	with open(csv_file, mode='r') as source:
		# read row
		for line in csv.DictReader(source):
			# add elements of the row as dictionary to list data 
			data.append(line)
	return data


# import all values related to trees, chakras, mouvement, sounds...
csv_file = 'data_attunement_generator.csv'
data_general = reading_data_as_list_of_dict(csv_file)

chakra_1 = 1
chakra_2 = 7
chakra_3 = 1

# pick randomly row of data depending on chakra number
selection_1 = []
selection_2 = []
selection_3 = []
# pick randomly row of data depending on chakra_1
for dict in data_general:
	if dict['chakra_number'] == str(chakra_1):
		selection_1.append(dict)
choice_1 = random.choice(selection_1)
# pick randomly row of data depending on chakra_2
for dict in data_general:
	if dict['chakra_number'] == str(chakra_2):
		selection_2.append(dict)
choice_2 = random.choice(selection_2)
# pick randomly row of data depending on chakra_3
for dict in data_general:
	if dict['chakra_number'] == str(chakra_3):
		selection_3.append(dict)
choice_3 = random.choice(selection_3)


frequency = choice_1['chakra_frequency']
print(chakra_1, frequency)
primal = choice_1['chakra_primal_sound']
sound_primal = choice_1['recording_primal']
print(chakra_1, primal, sound_primal)
bija = choice_2['chakra_bija_mantra']
sound_bija = choice_2['recording_bija']
print(chakra_2, bija, sound_bija)
vowel = choice_3['chakra_vowel']
sound_vowel = choice_3['recording_vowel']
print(chakra_3, vowel, sound_vowel)
solfeggio = choice_3['chakra_solfeggio']
sound_solfeggio = choice_3['recording_solfeggio']
print(chakra_3, solfeggio, sound_solfeggio)


# create song based on individual sounds
song = [sound_primal, sound_bija, sound_vowel, sound_solfeggio]

print(song)


# # generate song
subprocess.call(['sox'] + song + ['out.mp3'])
new_song = AudioSegment.from_mp3('out.mp3')

play(new_song)
