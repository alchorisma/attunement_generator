Installation
================

	pip install flask pydub


* pydub: only used in the commandline interface
* flask: web interface


Running the server
=================

Once per session:

	export FLASK_APP=attunement_app.py
	export FLASK_ENV=development

Then:

	flask run

